#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <valarray>

// print out x and array ys
// also print a,b if given
void print(double x, std::valarray<double> ys, double a=0.0, double b=0.0, double c=0.0) { 
    int n  = ys.size();
    std::cout << std::fixed << std::setw(15) << std::setprecision(8) << x << "\t";
    for(int i=0; i<n; i++) {
        std::cout << std::setw(15) << ys[i] << "\t";
    }
    if(a != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << a << "\t";
    if(b != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << b << "\t";
    if(c != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << c;
    std::cout << std::endl;
}

// overload print for Question 2b)
void print(double x, double ys, double a=0.0, double b=0.0, double c=0.0) { 
    std::cout << std::fixed << std::setw(15) << std::setprecision(8) << x  << "\t";
    std::cout << std::fixed << std::setw(15) << std::setprecision(8) << ys << "\t";
    if(a != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << a << "\t";
    if(b != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << b << "\t";
    if(c != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << c;
    std::cout << std::endl;
}

// test function for f(x,y)
// y'  = x^2 + 2x + 5
// y'' = 2x + 2
// y'''= 2
std::valarray<double> test_func(double x, std::valarray<double> ys) { 
    std::valarray<double> fs(2); 
    fs[0] = x*x + 2*x + 5; 
    fs[1] = 0;
    return fs;
} 

// ODE given in Question 1b)
std::valarray<double> funcQ1b(double x, std::valarray<double> ys) { 
    std::valarray<double> fs(2); 
    fs[0] = -ys[0]*ys[0] - ys[1]; 
    fs[1] = 5*ys[0] - ys[1];
    return fs;
} 

// ODE for Question 2a)
// Equation (1-2) from Assignment 5: erf(2) = (2/sqrt(pi)) int^2_0 exp(-z^2) dz
// Rewrite as an ODE: dy/dx = (2/sqrt(pi)) exp(-x^2), y(0) = 0 for 0<x<2
std::valarray<double> funcQ2a(double x, std::valarray<double> ys) { 
    std::valarray<double> fs(1); 
    fs[0] = (2/sqrt(M_PI))*exp(-x*x);
    return fs;
} 

// ODE for Question 2b)
// dy/dx = (2/sqrt(pi)) exp(-x^2), y(0) = 0 for 0<x<2
double funcQ2b(double x) {
    return (2/sqrt(M_PI))*exp(-x*x);
}

// euler method for solving ODE: dy/dx = f(x,y)
// y(x+dx) = y(x) + dx*f(x,y)
// yi = initial value of all y's
// xi = initial x
// xf = final x
// dx = step size
void euler(std::valarray<double> (*f)(double, std::valarray<double>), std::valarray<double> yi, double xi, double xf, double dx) {
    double x = xi;                  // initialise x
    std::valarray<double> ys(yi);   // initialise y
    std::valarray<double> fs;       // initialise f
    int n = ys.size();
    print(x,ys);
    
    while( x < xf ) {
        ys += dx * f(x,ys);         // update y
        x  += dx;                   // update x
        print(x,ys);
    }    
}

// 4th order Runge-Kutta method for solving ODE: dy/dx = f(x,y)
// y(x+dx) = y(x) + k1/6 + k2/3 + k3/3 + k4/6
//      k1 = dx f(x     , y(x)          )
//      k2 = dx f(x+dx/2, y(x) + dx k1/2)
//      k3 = dx f(x+dx/2, y(x) + dx k2/2)
//      k4 = dx f(x+dx  , y(x) + dx k3  )
// yi = initial value of all y's
// xi = initial x
// xf = final x
// dx = step size
void rk4(std::valarray<double> (*f)(double, std::valarray<double>), std::valarray<double> yi, double xi, double xf, double dx) {
    double x = xi;                  // initialise x
    std::valarray<double> ys(yi);   // initialise y
    int n = ys.size();
    
    // initialise k's for RK4
    std::valarray<double> k1;
    std::valarray<double> k2;
    std::valarray<double> k3;
    std::valarray<double> k4;

    print(x,ys);

    while( x < xf ) {

        if(x+dx > xf) dx = xf - x;  // make the program terminating exactly at xf

        // update k's
        k1 = dx*f(x         , ys         );
        k2 = dx*f(x + 0.5*dx, ys + 0.5*k1);
        k3 = dx*f(x + 0.5*dx, ys + 0.5*k2);
        k4 = dx*f(x + dx    , ys +     k3); 

        ys += (k1 + 2*k2 + 2*k3 + k4)/6; // update y
        x += dx;                         // update x

        print(x,ys);
    }    
}

// 5th order result, 4th order error estimate of Runge-Kutta method for solving ODE: dy/dx = f(x,y)
// with adaptive stepsize: dx(n+1) = dx(n) * ( delta(n+1) / delta(n) )^(1/5)
//
// y(x+dx) = y(x) + k1/6 + k2/3 + k3/3 + k4/6
//      k1 = dx f(x     , y(x)          )
//      k2 = dx f(x+dx/2, y(x) + dx k1/2)
//      k3 = dx f(x+dx/2, y(x) + dx k2/2)
//      k4 = dx f(x+dx  , y(x) + dx k3  )
//
// yi  = initial value of all y's
// xi  = initial x
// xf  = final x
// dx  = initial stepsize
// tol = tolerance (relative accuracy) (default = 10^-6)
void rk45(std::valarray<double> (*f)(double, std::valarray<double>), std::valarray<double> yi, double xi, double xf, double dx, double tol=1e-6) {

    // initise counters for numbers of functions and steps used in the calculation
    int counter_func = 0;
    int counter_step = 0;

    double x = xi;                          // initialise x
    std::valarray<double> ys(yi);           // initialise y
    std::valarray<double> y1(yi);           // initialise y1 (1-step estimate)
    std::valarray<double> y2(yi);           // initialise y2 (2-step estimate)
    std::valarray<double> delta(yi.size()); // initialise delta = y2 - y1
    double error = 0.0;                     // initialise error = error(delta_max)
    int n = ys.size();
    
    // initialise k's for RK4
    std::valarray<double> k1;
    std::valarray<double> k2;
    std::valarray<double> k3;
    std::valarray<double> k4;

    print(x,ys,dx);

    while( x < xf ) {

        counter_step++; // update counter

        if(x+dx >= xf) {
            dx = xf - x;  // make the program terminate exactly at xf
        }

        // update y1
        k1 = dx*f(x         , ys         );
        k2 = dx*f(x + 0.5*dx, ys + 0.5*k1);
        k3 = dx*f(x + 0.5*dx, ys + 0.5*k2);
        k4 = dx*f(x + dx    , ys +     k3); 
        y1 = ys + (k1 + 2*k2 + 2*k3 + k4)/6.0;

        // update y2 for half dx
        k1 = 0.5*k1;
        k2 = 0.5*dx*f(x + 0.25*dx, ys + 0.5*k1);
        k3 = 0.5*dx*f(x + 0.25*dx, ys + 0.5*k2);
        k4 = 0.5*dx*f(x + 0.5 *dx, ys +     k3); 
        y2 = ys + (k1 + 2*k2 + 2*k3 + k4)/6.0;
        // update y2 for another half dx
        k1 = 0.5*dx*f(x + 0.5 *dx, y2         );
        k2 = 0.5*dx*f(x + 0.75*dx, y2 + 0.5*k1);
        k3 = 0.5*dx*f(x + 0.75*dx, y2 + 0.5*k2);
        k4 = 0.5*dx*f(x +      dx, y2 +     k3); 
        y2 += (k1 + 2*k2 + 2*k3 + k4)/6.0; 

        counter_func += 11;

        delta = y2 - y1;     // update delta
        // define error of the current step as the largest (abslute) value of delta
        // divided by the average value of y1
        if(std::abs(delta.max()) > std::abs(delta.min())) {
            error = std::abs(delta.max()/(y1.sum()/n));
        } else {
            error = std::abs(delta.min()/(y1.sum()/n));
        }

        // check whether to accept or reject the current step
        if( error < tol ) {
            ys = y2 + delta/15.0; // update y
            x += dx;              // update x
            print(x,ys,dx,error);
        }
        // perform adaptive stepsize, dx_new = dx_old * (delta_desired/delta_old)^(1/5)
        dx *= 0.99*pow( tol/error, 0.2); // 0.99 is a safty factor 
    }    

    std::cout << "# number of functions evaluations = " << counter_func << std::endl;
    std::cout << "# number of steps                 = " << counter_step << std::endl;
}

// adaptive Simpson's rule
//
// y(x+dx) = y(x) + k1/6 + 2*k2/3 + k3/6
//      k1 = dx f(x)
//      k2 = dx f(x+dx/2)
//      k3 = dx f(x+dx)
//
// yi  = initial value of all y's
// xi  = initial x
// xf  = final x
// dx  = initial stepsize
// tol = tolerance (relative accuracy) (default = 10^-6)
void aSr(double (*f)(double), double yi, double xi, double xf, double dx, double tol=1e-6) {

    // initise counters for numbers of functions and steps used in the calculation
    int counter_func = 0;
    int counter_step = 0;

    double x  = xi;     // initialise x
    double ys = yi;     // initialise y
    double y1 = yi;     // initialise y1 (1-step estimate)
    double y2 = yi;     // initialise y2 (2-step estimate)
    double delta = 0.0; // initialise delta = y2 - y1
    double error = 0.0; // initialise error = abs(delta)
    
    // initialise k's for RK4
    double k1;
    double k2;
    double k3;

    print(x,ys,dx);

    while( x < xf ) {

        counter_step++; // update counter

        if(x+dx >= xf) {
            dx = xf - x;  // make the program terminate exactly at xf
        }

        // update y1
        k1 = dx*f(x);
        k2 = dx*f(x + 0.5*dx);
        k3 = dx*f(x + dx); 
        y1 = ys + (k1 + 4*k2 +k3)/6.0;

        // update y2 for half dx
        k1 = 0.5*k1;
        k2 = 0.5*dx*f(x + 0.25*dx);
        k3 = 0.5*dx*f(x + 0.5 *dx); 
        y2 = ys + (k1 + 4*k2 +k3)/6.0;
        // update y2 for another half dx
        k1 = 0.5*dx*f(x + 0.5 *dx);
        k2 = 0.5*dx*f(x + 0.75*dx);
        k3 = 0.5*dx*f(x + dx); 
        y2 += (k1 + 4*k2 +k3)/6.0;

        counter_func += 8;

        delta = y2 - y1;                // update delta
        error = std::abs(delta/y1);     // update error

        // check whether to accept or reject the current step
        if( error < tol ) {
            ys = y2 + delta/15.0; // update y
            x += dx;              // update x
            print(x,ys,dx,error);
        }
        // perform adaptive stepsize, dx_new = dx_old * (delta_desired/delta_old)^(1/5)
        dx *= 0.99*pow( tol/error, 0.2); // 0.99 is a safty factor 
    }    

    std::cout << "# number of functions evaluations = " << counter_func << std::endl;
    std::cout << "# number of steps                 = " << counter_step << std::endl;
}

int main(int argc, char *argv[])
{
    // To compile: g++ -o ode ode.cc

    // For Question 1b ***************************
    // run: ./ode 1 > output_q1b.txt
    if (*argv[1] == '1') {
        std::valarray<double> yi(2);
        yi[0] = 1.5;
        yi[1] = 1.5;
        rk45(funcQ1b, yi, 0.0, 10.0, 0.001,1e-6); 
    } 
    // result: 
    // y1(x=10) = -0.01310166
    // y2(x=10) = -0.00669902
    // number of functions evaluations = 2970
    // number of steps                 = 270
    // *******************************************
    
    // For Question 2a ***************************
    // run: ./ode 2 > output_q2a.txt
    if (*argv[1] == '2') {
        std::valarray<double> yi(1);
        yi[0] = 0;
        rk45(funcQ2a, yi, 0.0, 2.0, 0.001,1e-6); 
    }
    // result: erf(2) = 0.99532226
    // number of functions evaluations = 165
    // number of steps                 = 15
    // *******************************************
    
    // For Question 2b ***************************
    // run: ./ode 3 > output_q2b.txt
    if (*argv[1] == '3') {
        double yi = 0;
        aSr(funcQ2b, yi, 0.0, 2.0, 0.001,1e-6);
    } 
    // result: erf(2) = 0.99532226
    // number of functions evaluations = 120
    // number of steps                 = 15
    // *******************************************


    return 0;
}
