// machineAccuracy.cc 
// A program to find the machine accuracy of a single-precision floating point number

#include <iostream>
using namespace std;

int main() {
	// a variable to store the machine accuracy (or machine epsilon)
	float e = 0.5;
        float i = 1.0;

	// Find the machine epsilon
	while ( (i + e)  != i ) {
		e *= 0.5;
	}
        
        e *= 2;
        
        cout << "machine accuracy = " << e << endl;
        
	return 0;
}
