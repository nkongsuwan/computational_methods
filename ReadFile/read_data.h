#ifndef READ_DATA_H
#define READ_DATA_H

// Read tabulated data from a file
// The structure of the file should be
// two columns x and y without any comments

#include <fstream>
#include <vector>

class Read_Data
{
    public:
        // constructor
        Read_Data( std::istream & );
        double f( double ) const; // return f for given x
        double rf( double ) const; // return the closest x for given x

        // return the size of xy
        unsigned int size(  ) const;

        // return the last element
        double back(  ) const;

        // overload operator [] // deitel deitel how to programm 8th edition
        // subscript operator for objects; returs lvalue
        double &operator[]( int );

        // subscript operator for const objects; returs rvalue
        double operator[]( int ) const;

    private:
        std::vector<double> xy;
        double dx;
}; // end class Read_Data

#endif /* end of include guard: READ_DATA_H */
