#include "read_data.h"
#include <limits>
#include <iostream>
#include <cmath>

Read_Data::Read_Data( std::istream &input )
    : xy(std::istream_iterator<double>(input),
            std::istream_iterator<double>())
{
    dx = xy[2] - xy[0]; // see function f
} // end constructor Read_Data

double Read_Data::f( double x ) const
{
    // given that dx is the same for all data
    // we can find the index of an x as
    // x = x0 + dx * idx
    // idx = (x - x0) / dx
    // and since data is an array of 2 structures
    // xyxyxyxyxyxyxy...
    int idx = round(( x - xy[0] ) / dx);
    return xy[ 2 * idx + 1 ];
} // end function Read_Data::f

double Read_Data::rf( double x ) const
{
    int idx = round(( x - xy[0] ) / dx);
    return xy[ 2 * idx ];
} // end function rf ( reverse f )

unsigned int Read_Data::size(  ) const
{
    return xy.size();
}

double Read_Data::back(  )  const
{
    return xy.back();
}

double &Read_Data::operator[]( int subscript )
{
    return xy[ subscript ];
}

double Read_Data::operator[]( int subscript ) const
{
    return xy[ subscript ];
}
