#include <fstream>
#include <iostream>
#include <vector>
#include <limits>

int main()
{
    // Read Data
    std::ifstream myfile;
    // see at the end to see how this file is structured
    myfile.open("cubic_splines_natural.out");
    if ( !myfile ) {
        std::cerr << "Error while oppening input file\n";
        myfile.close();
        return -1;
        // or you can throw an exception here.e.g.
        // throw std::runtime_error("Error while oppening input file");
    }
    // skip the first line
    // the first parameter is the maximum characters you want to skip
    // the second parameter is until which character you want to skip
    // therefore here you skip for the maximum possible input characters
    // until the end of the line
    myfile.ignore( std::numeric_limits<std::streamsize>::max(), '\n' );
    std::vector<double> xy ((std::istream_iterator<double>(myfile)),
            std::istream_iterator<double>());
    myfile.close();

    // read the first 10 elements of your vector to verify that the reading
    // went ok
    // the vector has the following form
    // xyxyxyxyxyxyxyxy
    // Therefore e.g. if you need the 3rd y then you write
    // xy[ 3 * 2 + 1 ]
    // or the 3rd x
    // xy[ 3 * 2 ]
    std::cout << "\tx\t\ty" << std::endl;
    // for (unsigned int i = 0; i < xy.size(); i+=2)
    for (unsigned int i = 0; i < 20; i+=2)
        std::cout << xy[ i ] << "\t" << xy[ i + 1 ] << std::endl;
    std::cout << std::endl;

    myfile.close();
}

/*
The input file cubic_splines_natural.out has the following form in my case
x	y
-2.1 	0.012155
-2.0994099409941 	0.0122038742116551
-2.0988198819882 	0.0122527486753686
-2.0982298229823 	0.0123016236431988
-2.0976397639764 	0.0123504993672041
-2.0970497049705 	0.0123993760994428
-2.0964596459646 	0.0124482540919733
-2.0958695869587 	0.0124971335968539
-2.0952795279528 	0.0125460148661431
*/
