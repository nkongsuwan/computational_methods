set term png
set output "plot.png"

set xrange [-2.2:4]
set yrange [-0.01:1.01]
set xlabel "x" 
set ylabel "y"

plot "linear.txt" using 1:2 with lines lt 1 lw 1 lc rgb "red"  title "Linear Interpolation",\
     "natural_spline.txt" using 1:2 with lines lt 1 lw 1 lc rgb "blue" title "Cubic Spline"

set term x11