#include <iostream>
#include <cmath>
#include <vector>
#include <stdlib.h> 

using namespace std;

// function that returns the interpolated value y(x)  
double interpolate(double z, vector<double> x, vector<double> y) {
    int n = x.size();
    int j;
    if( z < x[0]   ) throw("input out of range: input < x[0]");
    if( z > x[n-1] ) throw("input out of range: input > x[n-1]");
    for(int i=0; i<=n-2; i++) {
        if(z < x[i+1]) {
            j = i;
            break;
        }
    }

    double A = (x[j+1] - z)/(x[j+1] - x[j]);
    double B = 1 - A;

    return A*y[j] + B*y[j+1];
}

int main(int arg, const char* argv[]) {
    
    // ************** INPUT **************************************************************************************************
    // Data
    double x_dat[] = {-2.1,-1.45,-1.3,-0.2,0.1,0.15,0.8,1.1,1.5,2.8,3.8};
    double y_dat[] = {0.012155,0.122151,0.184520,0.960789,0.990050,0.977751,0.527292,0.298197,0.105399,3.93669e-4,5.355348e-7};

    // ************** INPUT **************************************************************************************************

    // Vectors x and y containing the data point
    vector<double> x(begin(x_dat), end(x_dat));
    vector<double> y(begin(y_dat), end(y_dat));
    int n = x.size();

    //cout << interpolate(atof(argv[1]),x,y) << endl;
    int num_points = 100;
    for(int i = 0; i < num_points; i++) {
        double z = x[0] + i * (x[n-1] - x[0])/num_points;
        cout << z << "\t" << interpolate(z,x,y) << endl;
    }

    return 0;
}
