#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

// function to find y'' in My'' = F, where M is a tridiagonal matrix
// y'' = Y[0..n-1], a[0..n-1], b[0..n-1], c[0..n-1], and F[0..n-1]
void tridag(vector<double> &a, vector<double> &b, vector<double> &c, vector<double> &F, vector<double> &Y) {

    int j;
    int n = a.size();
    double bet;
    vector<double> gam(n); //One vector of workspace, gam, is needed.

    if (b[0] == 0.0) throw("Error 1 in tridag");
    //If this happens, then you should rewrite your equations as a set of order N, with u1 trivially eliminated.

    Y[0]=F[0]/(bet=b[0]);

    for (j=1;j<n;j++) { 
        //Decomposition and forward substitution.
        gam[j]=c[j-1]/bet;
        bet=b[j]-a[j]*gam[j];
        if (bet == 0.0) throw("Error 2 in tridag");
        Y[j]=(F[j]-a[j]*Y[j-1])/bet;
    }

    for (j=(n-2);j>=0;j--) {
        Y[j] -= gam[j+1]*Y[j+1]; //Backsubstitution.
    }
}

//function that takes x and y then gives y''
vector<double> compute_Y(vector<double> x, vector<double> y, double delta_1, double delta_n) {
    if ( x.size() != y.size() ) throw("x and y do not have the same length");
    int n = x.size();
    vector<double> a,b,c,F,Y(n);

    // i=0
    a.push_back(0); // undefined
    b.push_back( (x[1]-x[0])/3 );
    c.push_back( (x[1]-x[0])/6 );
    F.push_back( (y[1]-y[0])/(x[1]-x[0]) - delta_1 );    
    
    // from i=1 to i=n-2
    for(int i=1; i<=n-2; i++) {
        a.push_back( (x[i]  -x[i-1])/6 );
        b.push_back( (x[i+1]-x[i-1])/3 );
        c.push_back( (x[i+1]-x[i]  )/6 );
        F.push_back( (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]) );
    }

    // i=n-1
    a.push_back( (x[n-2]-x[n-1])/6 );   
    b.push_back( (x[n-2]-x[n-1])/3 );
    c.push_back(0); // undefined
    F.push_back( (y[n-1]-y[n-2])/(x[n-1]-x[n-2]) - delta_n );
    
    // Find y'' = Y
    tridag(a,b,c,F,Y);

    return Y;
}

//function that takes x and y then gives y'' for Natural Spline y''(x1) = y''(xn) = 0
vector<double> compute_Y(vector<double> x, vector<double> y) {
    if ( x.size() != y.size() ) throw("x and y do not have the same length");
    int n = x.size();
    vector<double> a,b,c,F,Y(n);

    // i=0
    a.push_back(0); // undefined
    b.push_back(1);
    c.push_back(0);
    F.push_back(0);    
    
    // from i=1 to i=n-2
    for(int i=1; i<=n-2; i++) {
        a.push_back( (x[i]  -x[i-1])/6 );
        b.push_back( (x[i+1]-x[i-1])/3 );
        c.push_back( (x[i+1]-x[i]  )/6 );
        F.push_back( (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]) );
    }

    // i=n-1
    a.push_back(0);   
    b.push_back(1);
    c.push_back(0); // undefined
    F.push_back(0);
    
    // Find y'' = Y
    tridag(a,b,c,F,Y);

    return Y;
}


// function that returns the interpolated value y(x) when y'(x1) and y'(xn) are specified
double interpolate(double z, vector<double> x, vector<double> y, vector<double> Y) {
    int n = x.size();
    int j;
    if( z < x[0]   ) throw("input out of range: input < x[0]");
    if( z > x[n-1] ) throw("input out of range: input > x[n-1]");
    for(int i=0; i<=n-2; i++) {
        if(z < x[i+1]) {
            j = i;
            break;
        }
    }

    double A = (x[j+1] - z)/(x[j+1] - x[j]);
    double B = 1 - A;
    double C = (pow(A,3) - A)*pow(x[j+1] - x[j], 2)/6;
    double D = (pow(B,3) - B)*pow(x[j+1] - x[j], 2)/6;

    return A*y[j] + B*y[j+1] + C*Y[j] + D*Y[j+1];
}

int main(int arg, const char* argv[]) {

    // ************** INPUT **************************************************************************************************
    // Data
    double x_dat[] = {-2.1,-1.45,-1.3,-0.2,0.1,0.15,0.8,1.1,1.5,2.8,3.8};
    double y_dat[] = {0.012155,0.122151,0.184520,0.960789,0.990050,0.977751,0.527292,0.298197,0.105399,3.93669e-4,5.355348e-7};

    // Boundary Condition
    // delta_1 = y'(x1), delta_n = y'(xn)
    double delta_1 = 0;
    double delta_n = 0;
    // ************** INPUT **************************************************************************************************

    // Vectors x and y containing the data point
    vector<double> x(begin(x_dat), end(x_dat));
    vector<double> y(begin(y_dat), end(y_dat));
    int n = x.size();

    // Y represent y''
    //vector<double> Y = compute_Y(x,y); // Natural Spline
    vector<double> Y = compute_Y(x,y,delta_1,delta_n); // y'(x1) and y'(xn) specified
    
 
    //int num_points = 100;
    //for(int i = 0; i < num_points; i++) {
    //    double z = x[0] + i * (x[n-1] - x[0])/num_points;
    //    cout << z << "\t" << interpolate(z,x,y,Y) << endl;
    //}

    cout << interpolate(0.4,x,y,Y) << endl;
    cout << interpolate(-0.128,x,y,Y) << endl;
    cout << interpolate(-2.0,x,y,Y) << endl;
    cout << interpolate(3.2,x,y,Y) << endl;    

    return 0;
}
