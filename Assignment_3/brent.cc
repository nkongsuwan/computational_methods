#include <iostream>
#include <cmath>
#include <iomanip>
#include <stdexcept>
#include <ctime>
#include <limits>
#include "cubicSpline.h"

using namespace std;

// function declaration
double testFunc(double);
double brent(double (*)(double), double, double, double tol=1e-8);
double brent(CubicSpline, double, double, double tol=1e-8);

// main
int main(int argc, char* argv[]) {

    cout << "test function" << endl;
    cout << "root 1: " << setprecision(10) << brent(testFunc,-5,-2) << endl;
    cout << "root 2: " << setprecision(10) << brent(testFunc, 2, 7) << endl;
    cout << "root 3: " << setprecision(10) << brent(testFunc,10,13,1e-3) << endl;
    cout << endl;

    CubicSpline cp("data.txt", 0, 0); // cubic spline with zeros first derivatives at the boundaries  
   
    // find two roots 
    cout << "cubic spline interpolation" << endl;   

    clock_t begin = clock();
    brent(cp, 1.9, 2.2);
    clock_t end1 = clock();
    brent(cp, 2.6, 2.9);
    clock_t end2 = clock(); 
    
    cout << "root 1: " << setprecision(10) << brent(cp, 1.9, 2.2) << endl; //2.065313589
    cout << "time taken: " << double(end1 - begin)/CLOCKS_PER_SEC << " s" << endl; // 2.1e-5 s
    cout << "root 2: " << setprecision(10) << brent(cp, 2.6, 2.9) << endl; //2.780719724
    cout << "time taken: " << double(end2 - end1)/CLOCKS_PER_SEC << " s" << endl; // 4e-6 s 

    return 0;
}

// test function
double testFunc(double x) {

    return (x+3.123456789)*(x-5.123456789)*(x-11.123456789);

}

// compute brent method for a given function
// l = left bracket, r = right bracket, tol = tolerance (= 1e-8 by default)
double brent(double (*f)(double), double l, double r, double tol) {
    
    // Note that in the lecture we use "b" as the current guess, "c" as the contrapoint and "a" as the previous guess
    // Here I use "b" as the current best guess "b(n)", "a" as the contrapoint and "c" and the previous guess "b(n-1)"
    // and "d" as the two previous guess "b(n-2)"
    double a  = l;
    double b  = r;
    double fa = f(a);
    double fb = f(b);
    double c, d, s, fc, fs=1.0; // random non-zero assignment to fs
    bool mflag = true; // A flag that tells whether the previous step is an interpolation or a bisection
                       // this flag is set to true for the 1st step

    if ( fa*fb >= 0 ) throw runtime_error("root is not bracketed");
    if ( abs(fa) < abs(fb) ) { swap(a,b); swap(fa,fb); } // if a is the better guess than b then swap a and b

    c = a; fc = fa;
    while ( fb!=0 && abs(b - a) > tol ) /* stop when the solution is found or within the tolerance */ {

        if ( fa!=fc && fb!=fc) {
            // inverse quadratic interpolation
            s = a*fb*fc/((fa-fb)*(fa-fc)) + b*fa*fc/((fb-fa)*(fb-fc)) + c*fa*fb/((fc-fa)*(fc-fb));
        } else {
            // secant method (used when two of the three points are degenerate)
            s = b - fb*(b-a)/(fb-fa);
        }
        
        // 5 conditions to determine whether the bisection method is necessary
        if ( ( s > max(0.25*(3*a+b),b) || s < min(0.25*(3*a+b),b)      ) ||
             (  mflag && abs(s-b) >= abs(b-c)*0.5                      ) ||
             ( !mflag && abs(s-b) >= abs(c-d)*0.5                      ) ||
             (  mflag && abs(b-c) <  numeric_limits<double>::epsilon() ) ||                          
             ( !mflag && abs(c-d) <  numeric_limits<double>::epsilon() ) ) {

            // perform bisection method instead of interpolation
            s = 0.5*(a+b);
            mflag = true;
        } else {
            // bisection is unnecessary
            mflag = false;
        }
        fs = f(s);
        // update c and d (b(n-1) and b(n-2))
        d = c; // d is assigned for the first time here
        c = b; fc = fb;
        
        // update a and b
        if ( fa*fs < 0) { b = s; fb = fs; } else { a = s; fa = fs; }
        if ( abs(fa) < abs(fb) ) { swap(a,b); swap(fa,fb); }
    }

    return b;
}

// compute brent method for a cubic spline interpolating function
// l = left bracket, r = right bracket, tol = tolerance (= 1e-8 by default)
double brent(CubicSpline cp, double l, double r, double tol) {

    double a  = l;
    double b  = r;
    double fa = cp.interpolate(a);
    double fb = cp.interpolate(b);
    double c, d, s, fc, fs=1.0; // random non-zero assignment to fs
    bool mflag = true;

    if ( fa*fb >= 0 ) throw runtime_error("root is not bracketed");
    if ( abs(fa) < abs(fb) ) { swap(a,b); swap(fa,fb); }

    c = a; fc = fa;
    while ( fb!=0 && abs(b - a) > tol ) {

        if ( fa!=fc && fb!=fc) {
            // inverse quadratic interpolation
            s = a*fb*fc/((fa-fb)*(fa-fc)) + b*fa*fc/((fb-fa)*(fb-fc)) + c*fa*fb/((fc-fa)*(fc-fb));
        } else {
            // secant method
            s = b - fb*(b-a)/(fb-fa);
        }

        if ( ( s > max(0.25*(3*a+b),b) || s < min(0.25*(3*a+b),b) ) ||
             (  mflag && abs(s-b) >= abs(b-c)*0.5                 ) ||
             ( !mflag && abs(s-b) >= abs(c-d)*0.5                 ) ||
             (  mflag && abs(b-c) <  tol                          ) ||                          
             ( !mflag && abs(c-d) <  tol                          ) ) {
            s = 0.5*(a+b);
            mflag = true;
        } else {
            mflag = false;
        }
        fs = cp.interpolate(s);
        d = c; // d is assigned for the first time
        c = b; fc = fb;
        
        if ( fa*fs < 0) { b = s; fb = fs; } else { a = s; fa = fs; }
        if ( abs(fa) < abs(fb) ) { swap(a,b); swap(fa,fb); }
    }

    return b;
}


