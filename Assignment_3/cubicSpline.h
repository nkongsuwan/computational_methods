#ifndef CUBIC_SPLINE
#define CUBIC_SPLINE

#include <fstream>
#include <cmath>
#include <vector>
#include <string>
#include <stdexcept>

using namespace std;

class CubicSpline {

    public:
        // constructors
        CubicSpline(string); // Natural Spline
        CubicSpline(string, double, double); // Specify 1st derivatives at the boundaries 
        
        double interpolate(double); // give interpolated value for a given x

    private:
        vector<double> x; // x data
        vector<double> y; // y data
        vector<double> Y; // y''
        int size; // size of data

        void tridag(vector<double> &, vector<double> &, vector<double> &, vector<double> &, vector<double> &); 
        // compute inverse matrix multiplication for tridiagonal matrix 
        void compute_Y(); // compute y'' for Natural Spline
        void compute_Y(double, double); // compute y'' for given first derivative at boundaries

};

#endif 
