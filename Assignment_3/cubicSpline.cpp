#include "cubicSpline.h"

// Constructor with Natural Spline
CubicSpline::CubicSpline(string fileName) {
 
    // Read data  
    ifstream myfile;
    myfile.open(fileName);
    if(!myfile) {
        throw runtime_error("Error while opening input file");
        myfile.close();
    }
    vector<double> xy ((istream_iterator<double>(myfile)), istream_iterator<double>());
    myfile.close();    

    int n = xy.size();
    if (n%2 != 0) throw runtime_error("invalid input data: x.size() != y.size()");
    for(int i=0; i<n; i+=2) {
        x.push_back(xy[i  ]);
        y.push_back(xy[i+1]);
    }
    
    size = n/2;
    compute_Y();
}

// Constructor with specified first derivatives at the boundaries
CubicSpline::CubicSpline(string fileName, double delta_1, double delta_n) {
 
    // Read data  
    ifstream myfile;
    myfile.open(fileName);
    if(!myfile) {
        throw runtime_error("Error while opening input file");
        myfile.close();
    }
    vector<double> xy ((istream_iterator<double>(myfile)), istream_iterator<double>());
    myfile.close();    

    int n = xy.size();
    if (n%2 != 0) throw runtime_error("invalid input data: x.size() != y.size()");
    for(int i=0; i<n; i+=2) {
        x.push_back(xy[i  ]);
        y.push_back(xy[i+1]);
    }
    
    size = n/2;
    compute_Y(delta_1, delta_n);
}

// return the interpolated value for given input z
double CubicSpline::interpolate(double z) {

    int j;
    if( z < x[0]   ) throw("input out of range: input < x[0]");
    if( z > x[size-1] ) throw("input out of range: input > x[n-1]");
    for(int i=0; i<=size-2; i++) {
        if(z < x[i+1]) {
            j = i;
            break;
        }
    }

    double A = (x[j+1] - z)/(x[j+1] - x[j]);
    double B = 1 - A;
    double C = (pow(A,3) - A)*pow(x[j+1] - x[j], 2)/6;
    double D = (pow(B,3) - B)*pow(x[j+1] - x[j], 2)/6;

    return A*y[j] + B*y[j+1] + C*Y[j] + D*Y[j+1];
}

void CubicSpline::tridag(vector<double> &a, vector<double> &b, vector<double> &c, vector<double> &F, vector<double> &Y) {

    double bet;
    vector<double> gam(size); //One vector of workspace, gam, is needed.

    if (b[0] == 0.0) throw("Error 1 in tridag");
    //If this happens, then you should rewrite your equations as a set of order N, with u1 trivially eliminated.

    Y.push_back(F[0]/(bet=b[0]));

    for (int j=1; j<size ;j++) { 
        //Decomposition and forward substitution.
        gam[j] = c[j-1]/bet;
        bet    = b[j]-a[j]*gam[j];
        
        if (bet == 0.0) throw("Error 2 in tridag");
        Y.push_back((F[j]-a[j]*Y[j-1])/bet);
    }

    for (int j=(size-2); j>=0; j--) {
        Y[j] -= gam[j+1]*Y[j+1]; //Backsubstitution.
    }
}

void CubicSpline::compute_Y() {

    vector<double> a,b,c,F;

    // i=0
    a.push_back(0); // undefined
    b.push_back(1);
    c.push_back(0);
    F.push_back(0);    
    
    // from i=1 to i=n-2
    for(int i=1; i<=size-2; i++) {
        a.push_back( (x[i]  -x[i-1])/6 );
        b.push_back( (x[i+1]-x[i-1])/3 );
        c.push_back( (x[i+1]-x[i]  )/6 );
        F.push_back( (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]) );
    }

    // i=n-1
    a.push_back(0);   
    b.push_back(1);
    c.push_back(0); // undefined
    F.push_back(0);
    
    // Find y'' = Y
    tridag(a,b,c,F,Y);
}

void CubicSpline::compute_Y(double delta_1, double delta_n) {

    vector<double> a,b,c,F;

    // i=0
    a.push_back(0); // undefined
    b.push_back( (x[1]-x[0])/3 );
    c.push_back( (x[1]-x[0])/6 );
    F.push_back( (y[1]-y[0])/(x[1]-x[0]) - delta_1 );    
    
    // from i=1 to i=n-2
    for(int i=1; i<=size-2; i++) {
        a.push_back( (x[i]  -x[i-1])/6 );
        b.push_back( (x[i+1]-x[i-1])/3 );
        c.push_back( (x[i+1]-x[i]  )/6 );
        F.push_back( (y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]) );
    }

    // i=n-1
    a.push_back( (x[size-2]-x[size-1])/6 );   
    b.push_back( (x[size-2]-x[size-1])/3 );
    c.push_back(0); // undefined
    F.push_back( (y[size-1]-y[size-2])/(x[size-1]-x[size-2]) - delta_n );
    
    // Find y'' = Y
    tridag(a,b,c,F,Y);
}
