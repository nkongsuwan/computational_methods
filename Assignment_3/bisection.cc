#include <iostream>
#include <cmath>
#include <iomanip>
#include <stdexcept>
#include "cubicSpline.h"

using namespace std;

// function declaration
double testFunc(double);
double bisection(double (*)(double), double, double, double tol=1e-8);
double bisection(CubicSpline, double, double, double tol=1e-8);

// main
int main(int argc, char* argv[]) {

    cout << "test function" << endl;
    cout << setprecision(10) << bisection(testFunc,-5,-2) << endl;   
    cout << setprecision(10) << bisection(testFunc, 2, 7) << endl;
    cout << setprecision(10) << bisection(testFunc,10,13,1e-3) << endl;
    cout << endl;

    CubicSpline cp("data.txt", 0, 0); // cubic spline with zeros first derivatives at the boundaries  
   
    // find two roots 
    cout << "cubic spline interpolation" << endl;

    clock_t begin = clock();
    bisection(cp, 1.9, 2.2); 
    clock_t end1 = clock();
    bisection(cp, 2.6, 2.9);
    clock_t end2 = clock();

    cout << "root 1: " << setprecision(10) << bisection(cp, 1.9, 2.2) << endl; //2.065313581
    cout << "time taken: " << double(end1 - begin)/CLOCKS_PER_SEC << " s" << endl; // 2.5e-5 s
    cout << "root 2: " << setprecision(10) << bisection(cp, 2.6, 2.9) << endl; //2.780719724
    cout << "time taken: " << double(end2 - end1)/CLOCKS_PER_SEC << " s" << endl; // 1.1e-5 s
    
    return 0;
}

// test function
double testFunc(double x) {

    return (x+3.123456789)*(x-5.123456789)*(x-11.123456789);

}

// compute bisection method for a given function
// l = left bracket, r = right bracket, tol = tolerance (= 1e-8 by default)
double bisection(double (*f)(double), double l, double r, double tol) {

    if (f(l)*f(r) >= 0) throw runtime_error("root is not bracketed");

    double left    = l;
    double right   = r;
    double mid     = 0.5*(left + right);
    double old_mid = left;

    while (abs(mid - old_mid) > tol) { 
        if ( f(left)*f(mid) > 0 ) {
            left = mid;
        } else {
            right = mid;
        }
        old_mid = mid;
        mid = 0.5*(left + right);
    }
    
    return mid;
}

// compute bisection method for a cubic spline interpolating function
// l = left bracket, r = right bracket, tol = tolerance (= 1e-8 by default)
double bisection(CubicSpline cp, double l, double r, double tol) {

    if (cp.interpolate(l)*cp.interpolate(r) >= 0) throw runtime_error("root is not bracketed");

    double left    = l;
    double right   = r;
    double mid     = 0.5*(left + right);
    double old_mid = left;

    while (abs(mid - old_mid) > tol) {
        if ( cp.interpolate(left)*cp.interpolate(mid) > 0 ) {
            left = mid;
        } else {
            right = mid;
        }
        old_mid = mid;
        mid = 0.5*(left + right);
    }
    
    return mid;
}

