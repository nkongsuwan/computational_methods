#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <valarray>
#include <stdexcept>
#include <limits>
#include <ctime>

// print out x and array ys
// also print a,b if given
void print(double x, std::valarray<double> ys, double a=0.0, double b=0.0) {
    int n  = ys.size();
    std::cout << std::fixed << std::setw(15) << std::setprecision(8) << x << "\t";
    for(int i=0; i<n; i++) {
        std::cout << std::setw(15) << ys[i] << "\t";
    }
    if(a != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << a << "\t";
    if(b != 0) std::cout << std::fixed << std::setw(20) << std::setprecision(13) << b;
    std::cout << std::endl;
}

// ODE given in Question 1)
std::valarray<double> funcQ1(double x, std::valarray<double> ys) {
    std::valarray<double> fs(2); 
    fs[0] = -ys[0]*ys[0] - ys[1]; 
    fs[1] = 5*ys[0] - ys[1];
    return fs;
}

// 5th order result, 4th order error estimate of Runge-Kutta method for solving ODE: dy/dx = f(x,y)
// with adaptive stepsize: dx(n+1) = dx(n) * ( delta(n+1) / delta(n) )^(1/5)
//
// y(x+dx) = y(x) + k1/6 + k2/3 + k3/3 + k4/6
//      k1 = dx f(x     , y(x)          )
//      k2 = dx f(x+dx/2, y(x) + dx k1/2)
//      k3 = dx f(x+dx/2, y(x) + dx k2/2)
//      k4 = dx f(x+dx  , y(x) + dx k3  )
//
// yi  = initial value of all y's
// xi  = initial x
// xf  = final x
// dx  = initial stepsize
// tol = tolerance (relative accuracy) (default = 10^-6)
void rk45(std::valarray<double> (*f)(double, std::valarray<double>), std::valarray<double> yi, double xi, double xf, double dx, double tol=1e-6) {

    // initise counters for numbers of functions and steps used in the calculation
    int counter_func = 0;
    int counter_step = 0;

    double x = xi;                          // initialise x
    std::valarray<double> ys(yi);           // initialise y
    std::valarray<double> y1(yi);           // initialise y1 (1-step estimate)
    std::valarray<double> y2(yi);           // initialise y2 (2-step estimate)
    std::valarray<double> delta(yi.size()); // initialise delta = y2 - y1
    double error = 0.0;                     // initialise error = error(delta_max)
    int n = ys.size();
    
    // initialise k's for RK4
    std::valarray<double> k1;
    std::valarray<double> k2;
    std::valarray<double> k3;
    std::valarray<double> k4;

    print(x,ys,dx);

    while( x < xf ) {

        counter_step++; // update counter

        if(x+dx >= xf) {
            dx = xf - x;  // make the program terminate exactly at xf
        }

        // update y1
        k1 = dx*f(x         , ys         );
        k2 = dx*f(x + 0.5*dx, ys + 0.5*k1);
        k3 = dx*f(x + 0.5*dx, ys + 0.5*k2);
        k4 = dx*f(x + dx    , ys +     k3); 
        y1 = ys + (k1 + 2*k2 + 2*k3 + k4)/6.0;

        // update y2 for half dx
        k1 = 0.5*k1;
        k2 = 0.5*dx*f(x + 0.25*dx, ys + 0.5*k1);
        k3 = 0.5*dx*f(x + 0.25*dx, ys + 0.5*k2);
        k4 = 0.5*dx*f(x + 0.5 *dx, ys +     k3); 
        y2 = ys + (k1 + 2*k2 + 2*k3 + k4)/6.0;
        // update y2 for another half dx
        k1 = 0.5*dx*f(x + 0.5 *dx, y2         );
        k2 = 0.5*dx*f(x + 0.75*dx, y2 + 0.5*k1);
        k3 = 0.5*dx*f(x + 0.75*dx, y2 + 0.5*k2);
        k4 = 0.5*dx*f(x +      dx, y2 +     k3); 
        y2 += (k1 + 2*k2 + 2*k3 + k4)/6.0; 

        counter_func += 11;

        delta = y2 - y1;     // update delta
        // define error of the current step as the largest (abslute) value of delta
        // divided by the average value of y1
        if(std::abs(delta.max()) > std::abs(delta.min())) {
            error = std::abs(delta.max()/(y1.sum()/n));
        } else {
            error = std::abs(delta.min()/(y1.sum()/n));
        }

        // check whether to accept or reject the current step
        if( error < tol ) {
            ys = y2 + delta/15.0; // update y
            x += dx;              // update x
            print(x,ys,dx,error);
        }
        // perform adaptive stepsize, dx_new = dx_old * (delta_desired/delta_old)^(1/5)
        dx *= 0.99*pow( tol/error, 0.2); // 0.99 is a safty factor 
    }    

    std::cout << "# number of functions evaluations = " << counter_func << std::endl;
    std::cout << "# number of steps                 = " << counter_step << std::endl;
}

// RK45 for using inside shooting() method
// Similar to method rk45() above, but "NO" printing to the terminal
// And this method now returns y1[x=10]
double rk45_NoPrint(std::valarray<double> (*f)(double, std::valarray<double>), std::valarray<double> yi, double xi, double xf, double dx, double tol=1e-6) {

    // initise counters for numbers of functions and steps used in the calculation
    int counter_func = 0;
    int counter_step = 0;

    double x = xi;                          // initialise x
    std::valarray<double> ys(yi);           // initialise y
    std::valarray<double> y1(yi);           // initialise y1 (1-step estimate)
    std::valarray<double> y2(yi);           // initialise y2 (2-step estimate)
    std::valarray<double> delta(yi.size()); // initialise delta = y2 - y1
    double error = 0.0;                     // initialise error = error(delta_max)
    int n = ys.size();
    
    // initialise k's for RK4
    std::valarray<double> k1;
    std::valarray<double> k2;
    std::valarray<double> k3;
    std::valarray<double> k4;

    while( x < xf ) {

        counter_step++; // update counter

        if(x+dx >= xf) {
            dx = xf - x;  // make the program terminate exactly at xf
        }

        // update y1
        k1 = dx*f(x         , ys         );
        k2 = dx*f(x + 0.5*dx, ys + 0.5*k1);
        k3 = dx*f(x + 0.5*dx, ys + 0.5*k2);
        k4 = dx*f(x + dx    , ys +     k3); 
        y1 = ys + (k1 + 2*k2 + 2*k3 + k4)/6.0;

        // update y2 for half dx
        k1 = 0.5*k1;
        k2 = 0.5*dx*f(x + 0.25*dx, ys + 0.5*k1);
        k3 = 0.5*dx*f(x + 0.25*dx, ys + 0.5*k2);
        k4 = 0.5*dx*f(x + 0.5 *dx, ys +     k3); 
        y2 = ys + (k1 + 2*k2 + 2*k3 + k4)/6.0;
        // update y2 for another half dx
        k1 = 0.5*dx*f(x + 0.5 *dx, y2         );
        k2 = 0.5*dx*f(x + 0.75*dx, y2 + 0.5*k1);
        k3 = 0.5*dx*f(x + 0.75*dx, y2 + 0.5*k2);
        k4 = 0.5*dx*f(x +      dx, y2 +     k3); 
        y2 += (k1 + 2*k2 + 2*k3 + k4)/6.0; 

        counter_func += 11;

        delta = y2 - y1;     // update delta
        // define error of the current step as the largest (abslute) value of delta
        // divided by the average value of y1
        if(std::abs(delta.max()) > std::abs(delta.min())) {
            error = std::abs(delta.max()/(y1.sum()/n));
        } else {
            error = std::abs(delta.min()/(y1.sum()/n));
        }

        // check whether to accept or reject the current step
        if( error < tol ) {
            ys = y2 + delta/15.0; // update y
            x += dx;              // update x
        }
        // perform adaptive stepsize, dx_new = dx_old * (delta_desired/delta_old)^(1/5)
        dx *= 0.99*pow( tol/error, 0.2); // 0.99 is a safty factor 
    }    
    return ys[0]; // y1(x=10)
}

// compute brent method for a given function
// l = left bracket, r = right bracket, tol = tolerance (= 1e-6 by default)
double brent(double (*f)(double), double l, double r, double tol=1e-6) {
    
    // Note that in the lecture we use "b" as the current guess, "c" as the contrapoint and "a" as the previous guess
    // Here I use "b" as the current best guess "b(n)", "a" as the contrapoint and "c" and the previous guess "b(n-1)"
    // and "d" as the two previous guess "b(n-2)"
    double a  = l;
    double b  = r;
    double fa = f(a);
    double fb = f(b);
    double c, d, s, fc, fs=1.0; // random non-zero assignment to fs
    bool mflag = true; // A flag that tells whether the previous step is an interpolation or a bisection
    // this flag is set to true for the 1st step
    
    if ( fa*fb >= 0 ) throw std::runtime_error("root is not bracketed");
    if ( std::abs(fa) < std::abs(fb) ) { std::swap(a,b); std::swap(fa,fb); } // if a is the better guess than b then swap a and b
    
    c = a; fc = fa;
    while ( fb!=0 && std::abs(b - a) > tol ) /* stop when the solution is found or within the tolerance */ {
        
        if ( fa!=fc && fb!=fc) {
            // inverse quadratic interpolation
            s = a*fb*fc/((fa-fb)*(fa-fc)) + b*fa*fc/((fb-fa)*(fb-fc)) + c*fa*fb/((fc-fa)*(fc-fb));
        } else {
            // secant method (used when two of the three points are degenerate)
            s = b - fb*(b-a)/(fb-fa);
        }
        
        // 5 conditions to determine whether the bisection method is necessary
        if ( ( s > std::max(0.25*(3*a+b),b) || s < std::min(0.25*(3*a+b),b)      ) ||
            (  mflag && std::abs(s-b) >= std::abs(b-c)*0.5                      ) ||
            ( !mflag && std::abs(s-b) >= std::abs(c-d)*0.5                      ) ||
            (  mflag && std::abs(b-c) <  std::numeric_limits<double>::epsilon() ) ||
            ( !mflag && std::abs(c-d) <  std::numeric_limits<double>::epsilon() ) ) {
            
            // perform bisection method instead of interpolation
            s = 0.5*(a+b);
            mflag = true;
        } else {
            // bisection is unnecessary
            mflag = false;
        }
        fs = f(s);
        // update c and d (b(n-1) and b(n-2))
        d = c; // d is assigned for the first time here
        c = b; fc = fb;
        
        // update a and b
        if ( fa*fs < 0) { b = s; fb = fs; } else { a = s; fa = fs; }
        if ( std::abs(fa) < std::abs(fb) ) { std::swap(a,b); std::swap(fa,fb); }
    }
    
    return b;
}

// Shooting Method for solving Question 1
//      y1' = - y1^2 - y2
//      y2' = 5 y1   - y2
//      y1(x=0)  = 3/2
// input specifies y2(x=0) 
// return y1(x=10)
double shooting(double y2) {
        std::valarray<double> yi(2);
        yi[0] = 1.5;
        yi[1] = y2;
        return rk45_NoPrint(funcQ1, yi, 0.0, 10.0, 0.001,1e-6);
}

int main(int argc, char *argv[])
{
    // To compile: g++ -o ode ode2.cc

    // For Question 1 ****************************
    // run: ./ode > output.txt

    // Use brent and shooting methods to solve for y2(x=0)
    double y2 = brent(shooting,-10.0,-1.0);
    // Use RK45 again to print the final result
    std::valarray<double> yi(2);
    yi[0] = 1.5;
    yi[1] = y2;
    rk45(funcQ1, yi, 0.0, 10.0, 0.001,1e-6);

    return 0;
}
