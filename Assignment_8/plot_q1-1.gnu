reset
set terminal png
set output "question1-1.png"

set xlabel "x"
set ylabel "y"

plot "output.txt" u 1:2 title "y1(x)", "output.txt" u 1:3 title "y2(x)
