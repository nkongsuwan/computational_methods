reset
set terminal png
set output "question1-2.png"

set xlabel "y1"
set ylabel "y2"

plot "output.txt" u 2:3 title "y1 vs y2"
