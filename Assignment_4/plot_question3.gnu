reset
set terminal png
set output "plot_question3.png"

set xlabel "y * 100"
set ylabel "frequency"
set xrange[-1:315]
set yrange [0:700]
set style data histogram
set style histogram cluster gap 1 
set style fill solid
set boxwidth 0.1

plot 'output_question3.txt' u 1 lc "red" title ""
