#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include "ranq1.h"

int main(int argc, char *argv[])
{
    Ranq1 R; // Initialise the random number generator
    std::vector<int> hist(100); // a vector of hundred zeros, used to make a histogram
    
    for (int i = 0; i < 1e5; ++i) { // random 1e5 number
        hist[floor(R.doub() * 100)]++; // update histogram
    }

    for (int i = 0; i < 100; i++) {
        std::cout << hist[i] << std::endl;
    }

    return 0;
}

