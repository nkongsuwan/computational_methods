#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include "ranq1.h"

double cdf_inverse(double x) { return acos(1-2*x);} // function to calculate an cdf^(-1)(x)

int main(int argc, char *argv[])
{
    Ranq1 R; // Initialise the random number generator
    int size = 315; // size of the histogram
    std::vector<int> hist(size); // a vector of zeros, used to make a histogram
    
    for (int i = 0; i < 1e5; ++i) { // random 1e5 number
        hist[floor(cdf_inverse(R.doub()) * 100)]++; // update histogram
    }

    for (int i = 0; i < size; i++) {
        std::cout << hist[i] << std::endl;
    }

    return 0;
}

