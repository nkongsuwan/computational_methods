reset
set terminal png
set output "plot_question1.png"

set xlabel "x * 100"
set ylabel "frequency"
set xrange[-1:101]
set yrange [0:1100]
set style data histogram
set style histogram cluster gap 1 
set style fill solid
set boxwidth 0.1

plot 'output_question1.txt' u 1 lc "red" title ""
