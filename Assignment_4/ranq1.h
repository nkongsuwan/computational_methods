#ifndef RANQ_1
#define RANQ_1

#include <cmath>

// A random number generator based on equation 7.1.3
// Ref. Numerical Recipes 3rd Edition, section 7.1.3
class Ranq1 {

    public:
        // constructors
        Ranq1(); // Default seed = 4101842887655102017LL
        Ranq1(unsigned long long int); // input seed
        
        unsigned long long int int64(); // generate a random integer between 0 and 2^64 exclusive
        double doub(); // generate a random real number between 0 and 1 exclusive

    private:
        unsigned long long int v;       // current value

};

#endif 
