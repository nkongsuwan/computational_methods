// question 3

#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include "ranq1.h"

double pdf(double y) { return (2/M_PI)*pow(sin(y),2); } // the desired pdf
double c(double y) { return (2/M_PI)*sin(y); } // the comparison function
double cdf_inverse(double x) { return acos(1-(M_PI*0.5)*x);} // function to calculate an cdf^(-1)(x)

int main(int argc, char *argv[])
{
    Ranq1 R; // Initialise the random number generator
    int size = 315; // size of the histogram
    std::vector<int> hist(size); // a vector of zeros, used to make a histogram
    
    int count = 0;  // for terminating condition
    int count_reject = 0; // for seeing how many random number are rejected 
    while (count < 1e5) { // random 1e5 numbers
        double y0 = cdf_inverse( (4/M_PI)*R.doub() ); // draw a random y0 between 0 and pi

        if ( c(y0)*R.doub() < pdf(y0) ) { // rejection method
            hist[floor( y0 * 100 )]++; // update histogram 
            count++; // update count
        } else {
            count_reject++;
        }

   }

    for (int i = 0; i < size; i++) {
        std::cout << hist[i] << std::endl;
    }
    std::cout << "count = " << count << std::endl;
    std::cout << "reject = " << count_reject << std::endl;

    return 0;
}

