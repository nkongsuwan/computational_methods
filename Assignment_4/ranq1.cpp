#include "ranq1.h"

// Default Constructor with default seed 
Ranq1::Ranq1() : v(4101842887655102017LL) {}

// input seed 
Ranq1::Ranq1(unsigned long long int s) : v(s)  {}

unsigned long long int Ranq1::int64() {
    v ^= v >> 21; 
    v ^= v << 35; 
    v ^= v >> 4;
    return v * 2685821657736338717LL;
}

double Ranq1::doub() { return 5.42101086242752217e-20 * int64(); }
