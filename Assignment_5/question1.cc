#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>

double test_function(double);   // polynomial function for simple test
double diff_erf(double);        // derivative of error function
double trapezium(double (*)(double), double, double, double tol=1e-6); // Trapezoidal rule for integration
double simpson(double (*)(double), double, double, double tol=1e-6);   // Simpson's rule for integration

int nt = 0; // global variable, used in trapezium()
int ns = 0; // global variable, used in simpson()
// n and m will be used to find out the number functions evaluated by trapezium() and simpson()

int main(int argc, char *argv[])
{
    std::cout << "Integrate x^2 + 2x + 3 from x=-5 to x=20" << std::endl;
    std::cout << "Exact           : " << std::setprecision(10) << 3158.33 << std::endl;
    std::cout << "Trapezoidal rule: " << std::setprecision(10) << trapezium(test_function,-5,20) << std::endl;  
    std::cout << "      number of function evaluations: " << nt << std::endl;
    std::cout << "Simpson's rule  : " << std::setprecision(10) << simpson(test_function,-5,20) << std::endl;
    std::cout << "      number of function evaluations: " << ns << std::endl;

    std::cout << "Integrate erf(x) from x=0 to x=2" << std::endl;
    std::cout << "Exact           : " << std::setprecision(10) << 0.9953222650 << std::endl;
    std::cout << "Trapezoidal rule: " << std::setprecision(10) << trapezium(diff_erf,0,2) << std::endl;  
    std::cout << "      number of function evaluations: " << nt << std::endl;
    std::cout << "Simpson's rule  : " << std::setprecision(10) << simpson(diff_erf,0,2) << std::endl;
    std::cout << "      number of function evaluations: " << ns << std::endl;

    return 0;
}

double test_function(double x) {
    return pow(x,2) + 2*x + 3;
}

double diff_erf(double x) {
    static double TwoDrootPi = 2/sqrt(M_PI);
    return TwoDrootPi*exp(-x*x);
}

// integrate function f from x=a to x=b within tolerance of tol (default=1e-6)
double trapezium(double (*f)(double), double a, double b, double tol) {
    double Tp = (b-a)*(0.5*f(a) + 0.5*f(b));     // previous estimate T(n-1)
    double Tc = 0.5*Tp + 0.5*(b-a)*f(0.5*(a+b)); // current estimate  T(n)
    int n = 2;  // counter for number of iteration
    nt = 3;     // counter for number of functions evaluated, declared globally above

    // while loop stops when the relative error is less than the tolerance
    while (std::abs(Tc/Tp-1) > tol) {         
        Tp = Tc; // update the previous estimate

        // Calculate new estimate, see section (b) in "Integration.pdf" for detail
        double F = 0;             // temporary variable
        double twoN = pow(2.0,n); // 2^(n-1), Note that the counter n hasn't been updated yet
        
        for(int i=1; i<twoN; i+=2) {  
            F += f( ( (twoN - i)*a + i*b )/twoN );
            nt++; // update counter
        }

        Tc = 0.5*Tc + (b-a)*( F )/twoN; //update current estimate
        n += 1; // update counter
    }

    return Tc;
}

double simpson(double (*f)(double), double a, double b, double tol) {
    
    double Tp = (b-a)*(0.5*f(a) + 0.5*f(b));     // previous estimate for trapezoidal rule T(n-1)
    double Tc = 0.5*Tp + 0.5*(b-a)*f(0.5*(a+b)); // current estimate for trapezoidal rule  T(n)
    double Sp = (4.0/3.0)*Tp;                    // previous estimate for Simpson's rule   S(n-2)
    double Sc = (4.0/3.0)*Tc - (1.0/3.0)*Tp;     // current estimate for Simpson's rule    S(n-1)
    int m = 2; // counter for number of iteration
    ns = 3;    // counter for number of function evaluated, declared globally above

    // while loop stops when the relative error is less than the tolerance
    while (std::abs(Sc/Sp-1) > tol) {         
        Tp = Tc; // update the previous estimate

        // Calculate new estimate, see section (b) in "Integration.pdf" for detail
        double F = 0;             // temporary variable
        double twoN = pow(2.0,m); // 2^(n-1), Note that the counter n hasn't been updated yet
        
        for(int i=1; i<twoN; i+=2) {  
            F += f( ( (twoN - i)*a + i*b )/twoN );
            ns++;
        }

        Tc = 0.5*Tc + (b-a)*( F )/twoN; //update current estimate
        Sp = Sc;
        Sc = (4.0/3.0)*Tc - (1.0/3.0)*Tp;
        m += 1; // update counter
    }

    return Sc;
}
