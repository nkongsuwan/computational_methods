#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>

int main(int argc, char *argv[])
{
    std::vector<double> v;
    v.push_back(1.0);
    v.push_back(2.0);
    v.push_back(3.0);
    v.push_back(4.0);
    v.push_back(5.0); 

    std::vector<double> w(v);
    w[3] = 100;

    for (int i = 0; i < w.size(); ++i) {
        std::cout << w[i] << std::endl;
    }

    for (int i = 0; i < v.size(); ++i) {
        std::cout << v[i] << std::endl;
    }
    return 0;
}
