#include <iostream>
#include <cmath>
#include <iomanip>
#include <vector>
#include <stdexcept>

int N_record = 0;
double randU() { return (double)rand()/RAND_MAX; } // uniform random number generator from 0 to 1
double func(std::vector<double>);                  // test function of multi variables
double diff_erf(std::vector<double>);              // derivative of error function, generalised to many dimensions
double pdfQ2ii(double);                              // the pdf given in Question 2ii
double monteCarloU(double (*)(std::vector<double>), int, double, double, double tol=1.0e-3); 
// Monte Carlo integration with uniform sampling distribution
double monteCarlo(double (*)(std::vector<double>), double (*)(double), int, double, double, double tol=1.0e-3); 
// Monte Carlo integration with user-specified sampling distribution

int N_last = 0; // global variable, number of samples in the last interation
int N_func = 0; // global variable, number of function evaluated

int main(int argc, char *argv[])
{
    srand(time(NULL)); // seed the random number generator 

    std::cout << "Question 1" << std::endl;
    std::cout << "Monte Carlo integration of sin(x0+x1+...+x7) from x=0 to x=pi/8 with uniform sampling distribution" << std::endl;
    std::cout << "Exact value = 537.1873411" << std::endl;
    std::cout << "Computed value = " << pow(10,6)*monteCarloU(func, 8, 0.0, M_PI/8.0,1.0e-6) << " (N = " << N_last << ")" << std::endl;
    std::cout << "Number of function evaluated = " << N_func << std::endl;

    std::cout << "Question 2" << std::endl;
    std::cout << "Monte Carlo integration of erf(x) from x=0 to x=2" << std::endl;
    std::cout << "Exact value = 0.9953222650" << std::endl;
    std::cout << "with uniform sampling distribution" << std::endl;
    std::cout << "Computed value = " << monteCarloU(diff_erf, 1, 0.0, 2.0,1.0e-6) << " (N = " << N_last << ")" << std::endl;
    std::cout << "Number of function evaluated = " << N_func << std::endl;
    std::cout << "with pdf(x) = -0.48x + 0.98" << std::endl;
    std::cout << "Computed value = " << monteCarlo(diff_erf, pdfQ2ii, 1, 0.0, 2.0,1.0e-6) << " (N = " << N_last << ")" << std::endl; 
    std::cout << "Number of function evaluated = " << N_func << std::endl;
}

// test function: sin(x0+x1+...+x7), all variables are contained in a vector xs 
double func(std::vector<double> xs) {
    double sumx = 0.0;
    int N = xs.size();
    for(int i=0; i<N; i++) 
    {
        sumx += xs[i];
    }
    return sin(sumx);
}

// (2/sqrt(pi))*exp(-x^2), generalised to many dimensions, i.e. x^2 = x0^2 + x1^2 + ...
double diff_erf(std::vector<double> xs) {
    static double TwoDrootPi = 2/sqrt(M_PI);
    double sumx2 = 0.0;
    int N = xs.size();
    for (int i = 0; i < N; ++i) {
        sumx2 += xs[i]*xs[i]; 
    }
    return TwoDrootPi*exp(-sumx2);
}

// a sampling distribution given in Question 2ii, pdf(x) = -0.48 x + 0.98 for 0<x<2
double pdfQ2ii(double x) {
    if (x < 0 || x > 2) throw std::invalid_argument( "received negative value" );
    return -0.48*x + 0.98;
}

// monteCarlo Integration using uniform distribution:
// integrand f, number of sample N, dimension dim, lower limit ll, upper limit ul, tolerance tol
double monteCarloU(double (*f)(std::vector<double>), int dim, double ll, double ul, double tol) {
    double Io = 0.0; // old value of integral, Iold
    double In = 0.0; // new value of integral, Inew
    int N = 1000;    // initial value of the number of samples, it will be later on increased automatically
    N_func = 0;      // reset the number of function evaluated
    std::vector<double> xs(dim,0); // define the vector for containing random numbers
    
    // start main loop for monte carlo integration
    do {
        double sumf = 0.0;

        for(int i=0; i<N; i++) {
            for(int j=0; j<dim; j++) { // generate random numbers for all dimension and store in xs
                xs[j] = ll + randU()*(ul - ll); // a random number between ll and ul
            }
            sumf += f(xs);
        }

        Io = In;                        // update Iold
        In = pow(ul - ll,dim)*sumf/N;   // update Inew
        N_func += N; // update counter for number of functions evaluated
        N *= 2;      // increase the number of samples for the next iteration 
    } while (std::abs(In-Io)/In > tol); // check whether the relative error is smaller than the tolerance

    N /= 2;     // the last increase of N is irrelavant
    N_last = N; // record the number of samples in the last interation
    return 0.5*(In + Io);
}

// monteCarlo Integration using a user-specified sampling distribution pdf(x)
// the random number with the given distribution is created by rejected method
// where comparison function c(x) = 1 (assume that pdf(x) < 1 for all x)
// integrand f, number of sample N, dimension dim, lower limit ll, upper limit ul, tolerance tol
double monteCarlo(double (*f)(std::vector<double>), double (*pdf)(double), int dim, double ll, double ul, double tol) {
    double Io = 0.0; // old value of integral, Iold
    double In = 0.0; // new value of integral, Inew
    int N = 1000;    // initial value of the number of samples, it will be later on increased automatically
    N_func = 0;      // reset the number of function evaluated, this value is defined globally above
    std::vector<double> xs(dim,0); // define the vector for containing random numbers

    // start main loop for monte carlo integration 
    do {
        double sumf = 0.0;

        for(int i=0; i<N; i++) {
            double randPDF = -1; // a variable for a random number used in rejection method
                                 // arbitrarily set its initial value to -1
            double W = 1;        // Weight function W(x0,x1,x2,...) = W(x0)*W(x1)*W(x2)*...
                                 // set its initial value to 1
            for(int j=0; j<dim; j++) { // generate random numbers for all dimension and store in xs
                do { // rejection method using comparison function c(x)=1
                    randPDF = ll + randU()*(ul - ll);           // a random number between ll and ul
                    if(randU() > pdf(randPDF)) {randPDF = -1;}  // Accept or Reject (set randPDF bacl to -1)
                } while (randPDF == -1); // randPDF=-1 means rejection, create a new random number
                xs[j] = randPDF;
                W *= pdf(randPDF);
            }

            sumf += f(xs)/W;
        }
        Io = In;     // update Iold
        In = sumf/N; // update Inew
        N_func += N; // count for number of functions evaluated
        N *= 2;      // increase the number of samples for the next iteration 
    } while (std::abs(In-Io)/In > tol); // check whether the relative error is smaller than the tolerance

    N /= 2;     // the last increase of N is irrelavant
    N_last = N; // record the number of samples in the last interation
    return 0.5*(In + Io);
}
